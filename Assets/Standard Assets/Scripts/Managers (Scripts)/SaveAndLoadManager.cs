﻿using System;
using System.IO;
using UnityEngine;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace SpaceGame
{
	[ExecuteInEditMode]
	public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
	{
		public static int currentAccountIndex;
		public static SaveData saveData = new SaveData();
		public static Dictionary<string, object> saveDataEntries = new Dictionary<string, object>();
		public const string FILE_NAME = "Save Data";

		public static void SetValue (string key, object value)
		{
			saveDataEntries[key] = value;
		}

		public static T GetValue<T> (string key, T defaultValue = default(T))
		{
			object value;
			if (saveDataEntries.TryGetValue(key, out value))
				return (T) value;
			else
				return defaultValue;
		}
		
		public static void Save ()
		{
			saveData = new SaveData();
			saveData.entries = new SaveData.Entry[saveDataEntries.Count];
			int i = 0;
			foreach (KeyValuePair<string, object> keyValuePair in saveDataEntries)
			{
				saveData.entries[i] = new SaveData.Entry(keyValuePair.Key, keyValuePair.Value);
				i ++;
			}
			string fileName = Application.persistentDataPath + "/" + FILE_NAME;
			FileStream fileStream = new FileStream(fileName, FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, saveData);
			fileStream.Close();
		}
		
		public static void Load ()
		{
			string fileName = Application.persistentDataPath + "/" + FILE_NAME;
			if (!File.Exists(fileName))
				return;
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
			fileStream.Close();
			saveDataEntries.Clear();
			for (int i = 0; i < saveData.entries.Length; i ++)
			{
				SaveData.Entry saveDataEntry = saveData.entries[i];
				saveDataEntries.Add(saveDataEntry.name, saveDataEntry.value);
			}
		}

		[Serializable]
		public class SaveData
		{
			public Entry[] entries;
			public _Vector3 startPosition;
			public _Vector3 startEulerAngles;

			[Serializable]
			public struct Entry
			{
				public string name;
				public object value;

				public Entry (string name, object value)
				{
					this.name = name;
					this.value = value;
				}
			}
		}
	}
}