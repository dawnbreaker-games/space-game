﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.XR.Oculus.Input;
using UnityEngine.InputSystem;
using Extensions;
using System.Diagnostics;

namespace SpaceGame
{
	// public class InputManager : SingletonUpdateWhileEnabled<InputManager>
	public class InputManager : SingletonMonoBehaviour<InputManager>
	{
		// public Process process;
		public InputDevice inputDevice;
		public InputSettings settings;
		public static bool UsingGamepad
		{
			get
			{
				return Gamepad.current != null;
			}
		}
		public static bool UsingMouse
		{
			get
			{
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
				return Keyboard.current != null;
			}
		}
		public static bool LeftClickInput
		{
			get
			{
				if (UsingGamepad)
					return false;
				else
					return Mouse.current.leftButton.isPressed;
			}
		}
		public static bool RightClickInput
		{
			get
			{
				if (UsingGamepad)
					return false;
				else
					return Mouse.current.rightButton.isPressed;
			}
		}
		public static Vector2? MousePosition
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.position.ReadValue();
				else
					return null;
			}
		}
		public static Vector2? LeftThumbstick
		{
			get
			{
				if (LeftTouchController != null)
					return Vector2.ClampMagnitude(LeftTouchController.thumbstick.ReadValue(), 1);
				else
					return null;
			}
		}
		public static Vector2? RightThumbstick
		{
			get
			{
				if (RightTouchController != null)
					return Vector2.ClampMagnitude(RightTouchController.thumbstick.ReadValue(), 1);
				else
					return null;
			}
		}
		public static bool SubmitInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.aButton.isPressed;
				else
					return Keyboard.current.enterKey.isPressed;// || Mouse.current.leftButton.isPressed;
			}
		}
		public static Vector2 UIMovementInput
		{
			get
			{
				if (UsingGamepad)
					return Vector2.ClampMagnitude(Gamepad.current.leftStick.ReadValue(), 1);
				else
				{
					int x = 0;
					if (Keyboard.current.dKey.isPressed)
						x ++;
					if (Keyboard.current.aKey.isPressed)
						x --;
					int y = 0;
					if (Keyboard.current.wKey.isPressed)
						y ++;
					if (Keyboard.current.sKey.isPressed)
						y --;
					return Vector2.ClampMagnitude(new Vector2(x, y), 1);
				}
			}
		}
		public static bool LeftGripInput
		{
			get
			{
				return LeftTouchController != null && LeftTouchController.gripPressed.isPressed;
			}
		}
		public static bool RightGripInput
		{
			get
			{
				return RightTouchController != null && RightTouchController.gripPressed.isPressed;
			}
		}
		public static bool LeftTriggerInput
		{
			get
			{
				return LeftTouchController != null && LeftTouchController.triggerPressed.isPressed;
			}
		}
		public static bool RightTriggerInput
		{
			get
			{
				return RightTouchController != null && RightTouchController.triggerPressed.isPressed;
			}
		}
		public static bool LeftPrimaryButtonInput
		{
			get
			{
				return LeftTouchController != null && LeftTouchController.primaryButton.isPressed;
			}
		}
		public static bool RightPrimaryButtonInput
		{
			get
			{
				return RightTouchController != null && RightTouchController.primaryButton.isPressed;
			}
		}
		public static bool LeftSecondaryButtonInput
		{
			get
			{
				return LeftTouchController != null && LeftTouchController.secondaryButton.isPressed;
			}
		}
		public static bool RightSecondaryButtonInput
		{
			get
			{
				return RightTouchController != null && RightTouchController.secondaryButton.isPressed;
			}
		}
		public static bool LeftThumbstickClickedInput
		{
			get
			{
				return LeftTouchController != null && LeftTouchController.thumbstickClicked.isPressed;
			}
		}
		public static bool RightThumbstickClickedInput
		{
			get
			{
				return RightTouchController != null && RightTouchController.thumbstickClicked.isPressed;
			}
		}
		public static Vector3? HeadPosition
		{
			get
			{
				if (Hmd != null)
					return Hmd.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? HeadRotation
		{
			get
			{
				if (Hmd != null)
					return Hmd.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static Vector3? LeftHandPosition
		{
			get
			{
				if (LeftTouchController != null)
					return LeftTouchController.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? LeftHandRotation
		{
			get
			{
				if (LeftTouchController != null)
					return LeftTouchController.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static Vector3? RightHandPosition
		{
			get
			{
				if (RightTouchController != null)
					return RightTouchController.devicePosition.ReadValue();
				else
					return null;
			}
		}
		public static Quaternion? RightHandRotation
		{
			get
			{
				if (RightTouchController != null)
					return RightTouchController.deviceRotation.ReadValue();
				else
					return null;
			}
		}
		public static OculusHMD Hmd
		{
			get
			{
				return InputSystem.GetDevice<OculusHMD>();
			}
		}
		public static OculusTouchController LeftTouchController
		{
			get
			{
				return (OculusTouchController) OculusTouchController.leftHand;
			}
		}
		public static OculusTouchController RightTouchController
		{
			get
			{
				return (OculusTouchController) OculusTouchController.rightHand;
			}
		}

		public static float GetAxis (InputControl<float> positiveButton, InputControl<float> negativeButton)
		{
			return positiveButton.ReadValue() - negativeButton.ReadValue();
		}

		public static Vector2 GetAxis2D (InputControl<float> positiveXButton, InputControl<float> negativeXButton, InputControl<float> positiveYButton, InputControl<float> negativeYButton)
		{
			Vector2 output = new Vector2();
			output.x = positiveXButton.ReadValue() - negativeXButton.ReadValue();
			output.y = positiveYButton.ReadValue() - negativeYButton.ReadValue();
			output = Vector2.ClampMagnitude(output, 1);
			return output;
		}

		// public override void OnEnable ()
		// {
		// 	ProcessStartInfo processStartInfo = new ProcessStartInfo("C:/Users/gilea/Test/Builds/Test (Windows x64)/Test.exe");
		// 	processStartInfo.RedirectStandardInput = true;
		// 	processStartInfo.UseShellExecute = false;
		// 	process = Process.Start(processStartInfo);
		// }

		// public override void DoUpdate ()
		// {
		// 	InputEvent inputEvent = new InputEvent();
		// 	process.StandardInput.WriteLine();
		// }
		
		public enum InputDevice
		{
			KeyboardAndMouse,
			VR
		}

		// [Serializable]
		// public struct InputEvent
		// {
		// 	public Vector3? leftHandPosition;
		// 	public Vector3? rightHandPosition;
		// 	public Quaternion? leftHandRotation;
		// 	public Quaternion? rightHandRotation;

		// 	public InputEvent (Vector3? leftHandPosition, Vector3? rightHandPosition, Quaternion? leftHandRotation, Quaternion? rightHandRotation)
		// 	{
		// 		this.leftHandPosition = leftHandPosition;
		// 		this.rightHandPosition = rightHandPosition;
		// 		this.leftHandRotation = leftHandRotation;
		// 		this.rightHandRotation = rightHandRotation;
		// 	}
		// }
	}
}