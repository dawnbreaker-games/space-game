using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SpaceGame
{
	public class Player : SingletonUpdateWhileEnabled<Player>, IDestructable
	{
		[HideInInspector]
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		public Rigidbody rigid;
		public float flySpeed;
		public float rotateRate;
		public LayerMask whatKillsMe;
		public float stopDistance;
		public int lives;
		public float spawnTimeLimit;
		public BoxCollider spawnBoundsBoxCollider;
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public BulletPatternEntryDontCollideWithShooter[] bulletPatternEntriesDontCollideWithShooter = new BulletPatternEntryDontCollideWithShooter[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public Animator animator;
		public float minAngleBetweenCollisionNormalsToKill;
		bool dead;

		public override void Awake ()
		{
			base.Awake ();
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
			for (int i = 0; i < bulletPatternEntriesDontCollideWithShooter.Length; i ++)
			{
				BulletPatternEntryDontCollideWithShooter bulletPatternEntryDontCollideWithShooter = bulletPatternEntriesDontCollideWithShooter[i];
				bulletPatternEntriesDict.Add(bulletPatternEntryDontCollideWithShooter.name, bulletPatternEntryDontCollideWithShooter);
			}
		}

		public override void OnEnable ()
		{
			base.OnEnable ();
			hp = maxHp;
		}

		public override void DoUpdate ()
		{
			if (VRCameraRig.instance.leftHand.triggerInput)
			{
				MoveToward (VRCameraRig.instance.leftHand.trs.position);
				RotateToward (VRCameraRig.instance.leftHand.trs.forward);
			}
			else if (VRCameraRig.instance.rightHand.triggerInput)
			{
				MoveToward (VRCameraRig.instance.rightHand.trs.position);
				RotateToward (VRCameraRig.instance.rightHand.trs.forward);
			}
			else
				rigid.velocity = Vector3.zero;
		}
		
		public void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}

		void MoveToward (Vector3 position)
		{
			Vector3 desiriedMove = position - trs.position;
			Vector3 desiriedLocalMove = trs.InverseTransformDirection(desiriedMove);
			Vector3 localMove = desiriedLocalMove.normalized * 99999;
			localMove = Vector3.ClampMagnitude(localMove, flySpeed);
			Vector3 move = trs.TransformDirection(localMove);
			RaycastHit hit;
			if (Physics.Raycast(trs.position, move, out hit, move.magnitude, whatKillsMe))
			{
				Hazard hazard = hit.collider.GetComponent<Hazard>();
				Bullet bullet = hazard as Bullet;
				if (bullet != null)
					Destroy(bullet.gameObject);
				TakeDamage (hazard.damage);
			}
			rigid.velocity = Vector3.zero;
			if (desiriedMove.magnitude <= stopDistance * Level.instance.trs.lossyScale.x)
			{
				trs.position = position;
				localMove = Vector3.zero;
			}
			rigid.AddRelativeForce(localMove, ForceMode.VelocityChange);
		}

		void RotateToward (Vector3 direction)
		{
			trs.forward = Vector3.RotateTowards(trs.forward, direction, rotateRate * Mathf.Deg2Rad * Time.deltaTime, 0);
		}

		public void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, MaxHp);
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public void Death ()
		{
			lives --;
			if (lives < 0)
				_SceneManager.instance.RestartSceneWithoutTransition ();
			else
				GameManager.instance.StartCoroutine(SpawnRoutine ());
		}

		IEnumerator SpawnRoutine ()
		{
			gameObject.SetActive(false);
			float startTime = Time.time;
			while (true)
			{
				if (VRCameraRig.instance.leftHand.primaryButtonInput || VRCameraRig.instance.leftHand.secondaryButtonInput)
				// if (InputManager.LeftPrimaryButtonInput || InputManager.LeftSecondaryButtonInput)
				{
					Spawn (VRCameraRig.instance.leftHand.trs.position);
					yield break;
				}
				else if (VRCameraRig.instance.rightHand.primaryButtonInput || VRCameraRig.instance.rightHand.secondaryButtonInput)
				// else if (InputManager.RightPrimaryButtonInput || InputManager.RightSecondaryButtonInput)
				{
					Spawn (VRCameraRig.instance.rightHand.trs.position);
					yield break;
				}
				if (Time.time - startTime >= spawnTimeLimit)
				{
					Spawn (spawnBoundsBoxCollider.bounds.RandomPoint());
					yield break;
				}
				yield return new WaitForEndOfFrame();
			}
		}

		void Spawn (Vector3 position)
		{
			position = position.ClampComponents(spawnBoundsBoxCollider.bounds.min, spawnBoundsBoxCollider.bounds.max);
			trs.position = position;
			dead = false;
			gameObject.SetActive(true);
		}

		void OnCollisionStay (Collision coll)
		{
			ContactPoint[] contactPoints = new ContactPoint[0];
			coll.GetContacts(contactPoints);
			for (int i = 0; i < contactPoints.Length; i ++)
			{
				ContactPoint contactPoint = contactPoints[i];
				for (int i2 = i + 1; i2 < contactPoints.Length; i2 ++)
				{
					ContactPoint contactPoint2 = contactPoints[i2];
					if (Vector3.Angle(contactPoint.normal, contactPoint2.normal) >= minAngleBetweenCollisionNormalsToKill)
					{
						Death ();
						return;
					}
				}
			}
		}
	}
}