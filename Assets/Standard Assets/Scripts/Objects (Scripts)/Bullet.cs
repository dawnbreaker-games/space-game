﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SpaceGame
{
	public class Bullet : Hazard
	{
		public float range;
		public float duration;
		public Rigidbody rigid;
		public float moveSpeed;
		public AutoDespawnMode autoDespawnMode;
		public ObjectPool.RangedDespawn rangedDespawn;
		public ObjectPool.DelayedDespawn delayedDespawn;
		public new Collider collider;
		[HideInInspector]
		public bool dead;
		public static List<Bullet> instances = new List<Bullet>();
		
		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rigid == null)
					rigid = GetComponent<Rigidbody>();
				if (collider == null)
					collider = GetComponentInChildren<Collider>();
				return;
			}
#endif
			dead = false;
			if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
				rangedDespawn = ObjectPool.instance.RangeDespawn(prefabIndex, gameObject, trs, range);
			else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
				delayedDespawn = ObjectPool.instance.DelayDespawn(prefabIndex, gameObject, trs, duration);
			rigid.velocity = Vector3.zero;
			rigid.AddRelativeForce(Vector3.forward * moveSpeed);
			instances.Add(this);
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			StopAllCoroutines();
			if (ObjectPool.instance != null)
			{
				if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
					ObjectPool.instance.CancelRangedDespawn (rangedDespawn);
				else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
					ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			}
			dead = true;
			instances.Remove(this);
		}

		public override void OnCollisionEnter (Collision coll)
		{
			if (!dead)
			{
				base.OnCollisionEnter (coll);
				ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
			}
		}

		public enum AutoDespawnMode
		{
			DontAutoDespawn,
			RangedAutoDespawn,
			DelayedAutoDespawn
		}
	}
}