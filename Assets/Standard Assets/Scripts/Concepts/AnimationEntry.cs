using System;
using UnityEngine;

namespace SpaceGame
{
	[Serializable]
	public struct AnimationEntry
	{
		public string animatorStateName;
		public int layer;
		public Animator animator;

		public void Play ()
		{
			animator.Play(animatorStateName, layer);
		}
	}
}