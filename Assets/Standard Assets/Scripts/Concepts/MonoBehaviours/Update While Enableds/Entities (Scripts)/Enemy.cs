using UnityEngine;
using Extensions;
using System.Collections.Generic;

namespace SpaceGame
{
	public class Enemy : Entity, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
        public Collider[] colliders = new Collider[0];
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public BulletPatternEntryDontCollideWithShooter[] bulletPatternEntriesDontCollideWithShooter = new BulletPatternEntryDontCollideWithShooter[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public static List<Enemy> instances = new List<Enemy>();

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instances.Add(this);
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			instances.Remove(this);
		}

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			for (int i = 0; i < colliders.Length; i ++)
			{
				Collider collider = colliders[i];
				for (int i2 = i + 1; i2 < colliders.Length; i2 ++)
				{
					Collider collider2 = colliders[i2];
					Physics.IgnoreCollision(collider, collider2, true);
				}
			}
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
			for (int i = 0; i < bulletPatternEntriesDontCollideWithShooter.Length; i ++)
			{
				BulletPatternEntryDontCollideWithShooter bulletPatternEntryDontCollideWithShooter = bulletPatternEntriesDontCollideWithShooter[i];
				bulletPatternEntriesDict.Add(bulletPatternEntryDontCollideWithShooter.name, bulletPatternEntryDontCollideWithShooter);
			}
		}

		public void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandleAttacking ();
			base.DoUpdate ();
		}

		public virtual void HandleAttacking ()
		{
		}
	}
}