using UnityEngine;
using Extensions;

namespace SpaceGame
{
	public class Level : SingletonUpdateWhileEnabled<Level>
	{
		public Transform trs;
		public float gravity;
		public float defaultContactOffset;
		public Transform passingObjectsParent;
		public float passingObjectsMoveSpeed;
		public FloatRange sizeRange;

		public override void DoUpdate ()
		{
			VRCameraRig.Hand leftHand = VRCameraRig.instance.leftHand;
			VRCameraRig.Hand rightHand = VRCameraRig.instance.rightHand;
			if (leftHand.gripInput)
			{
				if (rightHand.gripInput)
					SetParent (VRCameraRig.instance.bothHandsAverageTrs);
				else
					SetParent (leftHand.trs);
			}
			else if (rightHand.gripInput)
				SetParent (rightHand.trs);
			else
				SetParent (null);
			passingObjectsParent.localPosition += Vector3.back * passingObjectsMoveSpeed * Time.deltaTime;
			trs.SetWorldScale (Vector3.one * sizeRange.Clamp(trs.lossyScale.x));
		}

		void SetParent (Transform parent)
		{
			// if (trs.parent == parent)
			// 	return;
			// if (trs.parent != null && parent == null)
			// {
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					Rigidbody bulletRigidbody = bullet.rigid;
					Vector3 localVelocity = bullet.trs.InverseTransformDirection(bulletRigidbody.velocity);
					bulletRigidbody.velocity = Vector3.zero;
					bulletRigidbody.AddRelativeForce(localVelocity, ForceMode.VelocityChange);
				}
				Physics.gravity = -trs.up * gravity / trs.lossyScale.x;
				Physics.defaultContactOffset = defaultContactOffset * trs.lossyScale.x;
			// }
			trs.SetParent(parent);
			// if (parent == null)
			// 	Time.timeScale = 1;
			// else
			// 	Time.timeScale = 0;
		}
	}
}