using TMPro;
using System;
using Extensions;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SpaceGame
{
	public class ProceduralEndlessLevel : EndlessLevel
	{
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public FollowWaypoints waypointFollowerPrefab;
		public TMP_Text scoreText;
		public TMP_Text bestScoreReachedText;
		public float score;
		public float scorePerSecond;
		public float scorePerDamage;
		public BuildingSpawnEntry[] buildingSpawnEntries = new BuildingSpawnEntry[0];
		public float BestScoreReached
		{
			get
			{
				return PlayerPrefs.GetFloat(name + " best score", 0);
			}
			set
			{
				PlayerPrefs.SetFloat(name + " best score", value);
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			bestScoreReachedText.text = "Best score: " + string.Format("{0:0}", BestScoreReached);
		}

		public override void DoUpdate ()
		{
			base.DoUpdate ();
			score += Time.deltaTime * scorePerSecond;
			scoreText.text = "Score: " + string.Format("{0:0}", score);
			for (int i = 0; i < enemySpawnEntries.Length; i ++)
			{
				EnemySpawnEntry enemySpawnEntry = enemySpawnEntries[i];
				int spawnCount = (int) enemySpawnEntry.spawnsOverTime.Evaluate(Time.timeSinceLevelLoad * enemySpawnEntry.timeMultiplier) - enemySpawnEntry.spawnedCount;
				for (int i2 = 0; i2 < spawnCount; i2 ++)
					SpawnEnemy (enemySpawnEntry);
				enemySpawnEntries[i].spawnedCount += spawnCount;
			}
			for (int i = 0; i < buildingSpawnEntries.Length; i ++)
			{
				BuildingSpawnEntry buildingSpawnEntry = buildingSpawnEntries[i];
				int spawnCount = (int) buildingSpawnEntry.spawnsOverTime.Evaluate(Time.timeSinceLevelLoad * buildingSpawnEntry.timeMultiplier) - buildingSpawnEntry.spawnedCount;
				for (int i2 = 0; i2 < spawnCount; i2 ++)
					SpawnBuilding (buildingSpawnEntry);
				buildingSpawnEntries[i].spawnedCount += spawnCount;
			}
		}

		Enemy SpawnEnemy (EnemySpawnEntry enemySpawnEntry)
		{
			Vector3 spawnPosition;
			SpawnZone spawnZone;
			do
			{
				spawnZone = enemySpawnEntry.spawnZones[Random.Range(0, enemySpawnEntry.spawnZones.Length)];
				spawnPosition = spawnZone.GetRect3D().GetRandomPoint();
			} while ((Player.instance.trs.position - spawnPosition).sqrMagnitude < enemySpawnEntry.minSpawnDistanceToPlayer * enemySpawnEntry.minSpawnDistanceToPlayer);
			Enemy enemy = ObjectPool.instance.SpawnComponent<Enemy>(enemySpawnEntry.prefab.prefabIndex, spawnPosition);
			FollowWaypoints waypointFollower = ObjectPool.instance.SpawnComponent<FollowWaypoints>(waypointFollowerPrefab, spawnPosition);
			spawnZone = enemySpawnEntry.spawnZones[Random.Range(0, enemySpawnEntry.spawnZones.Length)];
			Vector3 waypointPosition = spawnZone.GetRect3D().GetRandomPoint();
			waypointFollower.waypoints[0].trs.position = waypointPosition;
			spawnZone = enemySpawnEntry.spawnZones[Random.Range(0, enemySpawnEntry.spawnZones.Length)];
			waypointPosition = spawnZone.GetRect3D().GetRandomPoint();
			waypointFollower.waypoints[1].trs.position = waypointPosition;
			spawnZone = enemySpawnEntry.spawnZones[Random.Range(0, enemySpawnEntry.spawnZones.Length)];
			waypointPosition = spawnZone.GetRect3D().GetRandomPoint();
			waypointFollower.waypoints[2].trs.position = waypointPosition;
			waypointFollower.moveSpeed = enemy.moveSpeed;
			waypointFollower.trs.SetParent(passingObjectsParent);
			waypointFollower.trs.localScale = Vector3.one;
			waypointFollower.waypointsParentWhilePlaying = passingObjectsParent;
			waypointFollower.enabled = true;
			enemy.moveSpeed = 0;
			enemy.trs.SetParent(waypointFollower.trs);
			enemy.trs.localScale = Vector3.one;
			enemy.onTakeDamage += OnEnemyTakeDamage;
			return enemy;
		}

		Building SpawnBuilding (BuildingSpawnEntry buildingSpawnEntry)
		{
			SpawnZone spawnZone = buildingSpawnEntry.spawnZones[Random.Range(0, buildingSpawnEntry.spawnZones.Length)];
			Rect3D spawnRect3D = spawnZone.GetRect3D();
			spawnRect3D.size -= buildingSpawnEntry.size.SetY(0);
			Vector3 spawnPosition;
			while (true)
			{
				spawnPosition = spawnRect3D.GetRandomPoint();
				Rect3D buildingRect3D = new Rect3D(spawnPosition + Vector3.up * buildingSpawnEntry.size.y / 2, buildingSpawnEntry.size, trs.eulerAngles);
				
				break;
			}
			Building building = ObjectPool.instance.SpawnComponent<Building>(buildingSpawnEntry.prefab.prefabIndex, spawnPosition);
			building.trs.localScale = buildingSpawnEntry.size;
			building.trs.SetParent(trs);
			return building;
		}

		void OnEnemyTakeDamage (float amount)
		{
			ProceduralEndlessLevel proceduralEndlessLevel = Level.instance as ProceduralEndlessLevel;
			if (proceduralEndlessLevel != null)
				proceduralEndlessLevel.score += amount * scorePerDamage;
		}

		[Serializable]
		public struct SpawnZone
		{
			public Transform trs;
			public BoxCollider boxCollider;

			public Rect3D GetRect3D ()
			{
				return new Rect3D(boxCollider.bounds.center, boxCollider.size.Multiply(trs.lossyScale), trs.eulerAngles);
			}
		}

		[Serializable]
		public class SpawnEntry
		{
			public SpawnZone[] spawnZones;
			public int spawnedCount;
			public AnimationCurve spawnsOverTime;
			public float timeMultiplier;
		}

		[Serializable]
		public class SpawnEntry<T> : SpawnEntry
		{
			public T prefab;
		}

		[Serializable]
		public class EnemySpawnEntry : SpawnEntry<Enemy>
		{
			public float minSpawnDistanceToPlayer;
		}

		[Serializable]
		public class BuildingSpawnEntry : SpawnEntry<Building>
		{
			public Vector3 size;
		}
	}
}