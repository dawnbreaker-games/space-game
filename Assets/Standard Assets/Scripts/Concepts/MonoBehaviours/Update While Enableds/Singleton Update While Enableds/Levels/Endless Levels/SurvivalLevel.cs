using UnityEngine;
using System;
using Extensions;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using TMPro;

namespace SpaceGame
{
	public class SurvivalLevel : EndlessLevel
	{
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public TMP_Text currentTimeText;
		public TMP_Text bestTimeReachedText;
		public float BestTimeReached
		{
			get
			{
				return PlayerPrefs.GetFloat(name + " best time", 0);
			}
			set
			{
				PlayerPrefs.SetFloat(name + " best time", value);
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			bestTimeReachedText.text = "Best time: " + BestTimeReached;
		}

		public override void DoUpdate ()
		{
			base.DoUpdate ();
			currentTimeText.text = string.Format("{0:0.#}", Time.timeSinceLevelLoad);
			for (int i = 0; i < enemySpawnEntries.Length; i ++)
			{
				EnemySpawnEntry enemySpawnEntry = enemySpawnEntries[i];
				int spawnCount = (int) enemySpawnEntry.spawnsOverTime.Evaluate(Time.timeSinceLevelLoad) - enemySpawnEntry.spawnedCount;
				for (int i2 = 0; i2 < spawnCount; i2 ++)
					SpawnEnemy (enemySpawnEntry);
				enemySpawnEntry.spawnedCount += spawnCount;
			}
		}

		Enemy SpawnEnemy (EnemySpawnEntry enemySpawnEntry)
		{
			Vector3 spawnPosition;
			do
			{
				BoxCollider spawnBoxCollider = enemySpawnEntry.spawnBoxColliders[Random.Range(0, enemySpawnEntry.spawnBoxColliders.Length)];
				spawnPosition = spawnBoxCollider.bounds.RandomPoint();
			} while ((Player.instance.trs.position - spawnPosition).sqrMagnitude < enemySpawnEntry.minSpawnDistanceToPlayer * enemySpawnEntry.minSpawnDistanceToPlayer);
			Enemy enemy = ObjectPool.instance.SpawnComponent<Enemy>(enemySpawnEntry.enemyPrefab, spawnPosition);
			return enemy;
		}

		[Serializable]
		public struct EnemySpawnEntry
		{
			public Enemy enemyPrefab;
			public BoxCollider[] spawnBoxColliders;
			public float minSpawnDistanceToPlayer;
			public int spawnedCount;
			public AnimationCurve spawnsOverTime;
		}
	}
}