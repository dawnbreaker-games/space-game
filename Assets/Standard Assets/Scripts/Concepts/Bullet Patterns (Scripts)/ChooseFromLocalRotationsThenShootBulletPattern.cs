using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace SpaceGame
{
	[CreateAssetMenu]
	public class ChooseFromLocalRotationsThenShootBulletPattern : BulletPattern
	{
		public BulletPattern bulletPattern;
		public ChooseMethod chooseMethod;
		public Vector3[] localRotations;
		public float accuracy = 1;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			int indexToUse = -1;
			if (chooseMethod == ChooseMethod.Random)
				indexToUse = Random.Range(0, localRotations.Length);
			else if (chooseMethod == ChooseMethod.LoopForwards)
			{
				indexToUse = IndexOfCurrentRotationInArray(spawner) + 1;
				if (indexToUse == localRotations.Length)
					indexToUse = 0;
			}
			else if (chooseMethod == ChooseMethod.LoopBackwards)
			{
				indexToUse = IndexOfCurrentRotationInArray(spawner) - 1;
				if (indexToUse == -1)
					indexToUse = localRotations.Length - 1;
			}
			spawner.localEulerAngles = localRotations[indexToUse];
			Bullet[] output = bulletPattern.Shoot (spawner, bulletPrefab, positionOffset);
			return output;
		}

		public virtual int IndexOfCurrentRotationInArray (Transform spawner)
		{
			for (int i = 0; i < localRotations.Length; i ++)
			{
				if (Quaternion.Angle(Quaternion.Euler(localRotations[i]), Quaternion.Euler(spawner.localEulerAngles)) <= accuracy)
					return i;
			}
			return -1;
		}

        public enum ChooseMethod
        {
            Random,
            LoopForwards,
			LoopBackwards
        }
	}
}