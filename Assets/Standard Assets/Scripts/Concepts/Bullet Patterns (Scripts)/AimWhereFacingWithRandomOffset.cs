﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace SpaceGame
{
	[CreateAssetMenu]
	public class AimWhereFacingWithRandomOffset : AimWhereFacing
	{
		public FloatRange randomShootOffsetRange;
		
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return base.GetShootDirection(spawner).Rotate(Quaternion.Euler(Random.onUnitSphere * randomShootOffsetRange.Get(Random.value)));
		}
	}
}