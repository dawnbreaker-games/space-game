﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace SpaceGame
{
	[CreateAssetMenu]
	public class ShootAtPlayerWithOffsetThenBurstAndShootBulletPatternFromBulletAfterDelay : AimAtPlayer
	{
		public Vector3 shootOffset;
		public float burstDelay;
		public Bullet burstBulletPrefab;
		public BulletPattern bulletPattern;
		public float burstPositionOffset;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			Vector3 direction = Player.instance.trs.position - spawner.position;
			direction = direction.Rotate(Quaternion.Euler(shootOffset));
			Bullet bullet = ObjectPool.instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawner.position + direction * positionOffset, Quaternion.LookRotation(direction), Level.instance.trs);
			bullet.StartCoroutine(BurstRoutine (bullet));
			return new Bullet[1] { bullet };
		}

		IEnumerator BurstRoutine (Bullet bullet)
		{
			yield return new WaitForSeconds(burstDelay);
			ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
			bulletPattern.Shoot (bullet.trs, burstBulletPrefab, burstPositionOffset);
			yield break;
		}
	}
}