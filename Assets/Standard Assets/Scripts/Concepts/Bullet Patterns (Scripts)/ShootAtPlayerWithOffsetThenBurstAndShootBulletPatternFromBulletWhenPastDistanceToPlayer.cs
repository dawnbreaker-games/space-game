﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;

namespace SpaceGame
{
	[CreateAssetMenu]
	public class ShootAtPlayerWithOffsetThenBurstAndShootBulletPatternFromBulletWhenPastDistanceToPlayer : AimAtPlayer
	{
		public Vector3 shootOffset;
		public Bullet burstBulletPrefab;
		public BulletPattern bulletPattern;
		public float burstPositionOffset;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			Vector3 direction = Player.instance.trs.position - spawner.position;
			direction = direction.Rotate(Quaternion.Euler(shootOffset));
			Vector3 spawnPosition = spawner.position + direction * positionOffset;
			Bullet bullet = ObjectPool.instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawnPosition, Quaternion.LookRotation(direction), Level.instance.trs);
			bullet.StartCoroutine(BurstRoutine (spawnPosition, bullet));
			return new Bullet[1] { bullet };
		}

		IEnumerator BurstRoutine (Vector3 initPosition, Bullet bullet)
		{
			while (true)
			{
				if ((bullet.trs.position - initPosition).sqrMagnitude >= (Player.instance.trs.position - bullet.trs.position).sqrMagnitude)
				{
					ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
					bulletPattern.Shoot (bullet.trs, burstBulletPrefab, burstPositionOffset);
					yield break;
				}
				yield return new WaitForEndOfFrame();
			}
		}
	}
}